#include <stdio.h>

int main(void){
// Declarar variable nombre.
	char nombre[32];
// Declarar variable edad.
	unsigned int edad;
// Declarar variable nomina y asignar valor de 0.0.
	float nomina = 0;

// Preguntar a usuario nombre de empleado.
	printf("Ingresa nombre de empleado: ");
// Obtener nombre y asignarlo a variable nombre.
	scanf("%s", nombre);
// Preguntar a usuario por edad (0 para finalizar).
	printf("Ingresa edad de empleado (0 para finalizar): ");
// Obtener de usuario edad y asignar a variable edad.
	scanf("%u", &edad);

// Mientras edad sea mayor o igual a 18:
	while (edad >= 18){
// Declarar variable costoHora.
		float costoHora;
// Declarar variable dias.
		unsigned int dias;
// Declarar variable totalHoras y asignar valor de 0.
		unsigned int totalHoras = 0;
// Declarar variable pago y asignar valor de 0.0.
		float pago = 0;

// Preguntar a usuario por valor de hora.
		printf("Ingresa valor de hora: $");
// Obtener de usuario valor de hora y asignar a variable costoHora.
		scanf("%f", &costoHora);

// Preguntar a usuario por cantidad de días trabajados.
		printf("Ingresa cantidad de días trabajados: ");
// Obtener de usuario dias y asignar a variable dias.
		scanf("%u", &dias);

// Si dias es menor a 1 o mayor a 31:
		if (dias < 1 || dias > 31){
// Imprimo: Error, cantidad de días inválido.
			puts("Error, cantidad de días inválido");

// Preguntar a usuario nombre de empleado.
			printf("Ingresa nombre de empleado: ");
// Obtener nombre y asignarlo a variable nombre.
			scanf("%s", nombre);
// Preguntar a usuario por edad (0 para finalizar).
			printf("Ingresa edad de empleado (0 para finalizar): ");
// Obtener de usuario edad y asignar a variable edad.
			scanf("%u", &edad);
			}
// De lo contrario:
		else {
// Para jornal desde 1; hasta jornal <= dias; incremento jornal
//	 en 1:
			for (unsigned int jornal = 1; jornal <= dias; jornal++){
// Declarar variable horas.
				unsigned int horas;

// Preguntar cantidad de horas trabajadas:
				printf("Ingresa horas trabajas en día No. %u: ",
						jornal);
// Obtener de usuario cantidad de horas trabajadas y asignar a
//	    varaible horas.
				scanf("%u", &horas);

// Si horas es mayor a 0 y menor a 25:
				if ( horas > 0 && horas < 25){
// Sumar horas a totalHoras.
					totalHoras += horas;
// Declarar variable esNocturno.
					unsigned short int esNocturno;
// Preguntar a usuario por recargo nocturno (1 sí, 0 no):
					printf("¿Recargo nocturno? (1 -> Sí, 0"\
							" -> No): ");
// Obtener indicador de recargo nocturno y asignar a varaible
//	       esNocturno.
					scanf("%hu", &esNocturno);

// Si esNocturno es igual a 1:
					if (esNocturno){
// Calculo horas multiplicado por costoHora y recargo de
//		  7%, sumo resultado a pago.
						pago += horas * costoHora * 
							1.07;
					}
// De lo contrario:
					else{
// Calculo horas multiplicado por costoHora, sumo resultado
//		  a pago.
						pago += horas * costoHora;
					}
				}
// De lo contrario (cantidad de horas inválida):
				else{
// Imprimo: error, cantidad de horas inválida. 
					puts("Error, cantidad de horas "\
							"inválida");
// Decremento jornal en 1 para repetir día.
					jornal--;
				}
			}
// Si edad es mayor o igual a 50 y totalHoras es mayor a 60:
			if (edad >= 50 && totalHoras >= 60){
// Calculo incremento de 1.5% sobre pago.
				pago *= 1.015;
			}
// Imprimo nombre y pago.
			printf("Nombre: %s\nPago: $%.2f\n", nombre, pago);
// Sumar pago a acumulado de nomina.
			nomina += pago;
// Preguntar a usuario por nombre de empleado.
			printf("Ingresa nombre de empleado: ");
// Obtener nombre y asignarlo a variable nombre.
			scanf("%s", nombre);
// Preguntar a usuario por edad (0 para finalizar).
			printf("Ingresa edad de empleado (0 para finalizar): ");
// Obtener de usuario edad y asignar a variable edad.
			scanf("%u", &edad);
		}
	}

// Imprimir total de nomina.
	printf("Total nómina: $%.2f\n", nomina);
}
