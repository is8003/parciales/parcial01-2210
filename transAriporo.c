#include <stdio.h>

int main(void){

	// Inicializo variable pasEsp (pasajeros en espera) con valor 0;
	unsigned int pasEsp = 0;

	// Imprimo encabezado de tabla.
	printf ("%s\n", " Hora Trans.|Espe. Chiva|Van|Camp.");

	// Para hora desde 5; hasta hora menor o igual a 17; incremento hora
	//   en 1:
	for (unsigned int hora = 5; hora <= 17; hora++){
		// Inicializo variable pasTra (pasajeros transportados) con valor 0.
		unsigned int pasTra = 0;
		// Inicializo variable usoChi (uso de chiva) con valor 0.
		unsigned int usoChi = 0;
		// Inicializo variable usoVan (uso de van) con valor 0.
		unsigned int usoVan = 0;
		// Inicializo variable usoCam (uso de campero) con valor 0.
		unsigned int usoCam = 0;

		// Sumo hora * hora - 24 * hora + 144 a pasEsp.
		pasEsp += hora * hora - 24 * hora + 144;

		// Si pasEsp es mayor igual a 21:
		if (pasEsp >= 21){
			// Resto 21 a pasEsp.
			pasEsp -= 21;
			// Sumo 21 a pasTra.
			pasTra += 21;
			// Asigno 1 a usoChi.
			usoChi = 1;
		}

		// Si pasEsp es mayor igual a 11:
		if (pasEsp >= 11){
			// Resto 11 a pasEsp.
			pasEsp -= 11;
			// Sumo 11 a pasTra.
			pasTra += 11;
			// Asigno 1 a usoVan.
			usoVan = 1;
		}
		// Si pasEsp es mayor igual a 7:
		if (pasEsp >= 7){
			// Resto 7 a pasEsp.
			pasEsp -= 7;
			// Sumo 7 a pasTra.
			pasTra += 7;
			// Asigno 1 a usoCam.
			usoCam = 1;
		}

		// Imprimo hora , pasTra/pasEsp y usoChi/usoVan/usoCam.
		printf("%2u:00 %6u|%5u %5u|%3u|%4u\n", hora, pasTra, pasEsp, usoChi, 
				usoVan, usoCam);
	}

}
