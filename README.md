# Pago de nómina en empresa de Manufactura

En una empresa de manufactura se contratan trabajadores y se les paga por cada
hora trabajada un valor que puede ser diferente para cada trabajador.

El asistente de Recursos Humanos desea calcular el valor que debe pagar a cada
trabajador al final del mes y el total que la empresa debe desembolsar. El
asistente NO sabe de antemano cuantos trabajadores hay.

Para cada trabajador se conoce el nombre, la edad, el valor hora a pagar y la
cantidad de días que ha trabajado en el mes. Adicionalmente para cada día
trabajado por el empleado, se sabe cuántas horas trabajó en ese día y un
indicador de si esas horas tienen recargo nocturno; el recargo aplica para
todas las horas de ese día. El recargo nocturno es del 7% sobre el valor hora.

Además, la empresa tiene la política de que a los empleados que tienen más de
50 años se les paga un 1.5% adicional sobre el valor a pagar, siempre y cuando
hayan trabajado al menos 60 horas durante el mes.

Elabore un programa que ayude al asistente de Recursos Humanos a calcular todos
los valores que necesita; en el reporte para cada trabajador indique nombre y
valor a pagar.

## Refinamiento 1

1. Obtener nombre y edad.
1. Mientras edad sea mayor o igual a 18:
   1. Obtener valor hora y cantidad de días trabajados.
   1. Si días es menor a 1 0 mayor a 31:
      1. Imprimo: Error, cantidad de días inválido.
      1. Obtener nombre y edad.
   1. De lo contrario:
      1. Por cada día trabajado:
	 1. Obtener cantidad de horas trabajadas.
	 1. Si cantidad de horas es mayor a 0 y menor a 25:
	    1. Sumar horas a acumulado mensual.
	    1. Obtener indicador de recargo nocturno.
	    1. Si recargo nocturno está activo:
	       1. Calculo hora trabajada multiplicado por valor de hora y
		  recargo de 7%, sumo resultado a pago.
	    1. De lo contrario:
	       1. Calculo hora trabajada multiplicado por valor de hora, sumo
		  resultado a pago.
	 1. De lo contrario:
	    1. Imprimo: error, cantidad de horas inválida. 
	    1. Reduzco contador de día para preguntar mismo día nuevamente.
      1. Si edad es mayor o igual a 50 años y acumulado de horas es mayor a
	 `60`:
	 1. Calculo incremento de 1.5% sobre pago.
      1. Imprimo nombre y pago.
      1. Sumar pago a acumulado de nómina.
      1. Obtener nombre y edad.
1. Imprimir total de nómina.

## Refinamiento 2

```mermaid
graph TD
    ini(("Inicio"))-->getNomEda[/"Obtener: nombre<br>y edad"/]
    getNomEda-->edExitCheck{"edad > 18"}
    edExitCheck--"No"-->nomPrint[/"Imprimo: total<br>nómina"/]
    nomPrint-->fin(("Fin"))
    edExitCheck--"Sí"-->getDayCost[/"Obtener: costo<br>hora y días"/]
    getDayCost--"Sí"-->dayCheck{"dias < 1 ||<br>dias > 31"}
    dayCheck--"No"-->setJornal["jornal = 1"]
    setJornal-->jorDay{"jornal <= dias"}
    jorDay--"Sí"-->getHours[/"Obtener: horas"/]
    getHours-->hoursCheck{"horas > 0 &&<br>horas < 25"}
    hoursCheck--"Sí"-->sumHours["totalHoras += horas"]
    sumHours-->getNoc[/"Obtener: recargo<br>nocturno"/]
    getNoc-->nocCheck{"esNocturno == 1"}
    nocCheck--"Sí"-->payNoc["Calculo pago<br>con recargo"]
    payNoc-->jorInc["jornal++"]
    nocCheck--"No"-->pay["Calculo pago"]
    pay-->jorInc
    hoursCheck--"No"-->hourErr[/"Imprimo: Error, cantidad<br>de horas válidas"/]
    hourErr-->jorDec["jornal--"]
    jorDec-->jorInc
    jorInc-->hoursCheck
    jorDay--"No"-->loyCheck{"edad >= 50<br>&& horas >= 60"}
    loyCheck--"Sí"-->loyInc["Aumento bono<br>sobre pago"]
    loyInc-->resPrint[/"Imprimo: nombre<br>y pago"/]
    dayCheck--"Si"-->dayErr[/"Imprimo: error cantidad de<br>días inválido"/]
    dayErr-->getNomEda3[/"Obtener: nombre<br>y edad"/]
    getNomEda3-->edExitCheck
    loyCheck--"No"-->resPrint
    resPrint-->getNomEda2[/"Obtener: nombre<br>y edad"/]
    getNomEda2-->edExitCheck
```

1. Preguntar a usuario nombre de empleado.
1. Obtener nombre y asignarlo a variable `nombre`.
1. Preguntar a usuario por edad (0 para finalizar).
1. Obtener de usuario edad y asignar a variable `edad`.
1. Mientras `edad` sea mayor a `0`:
   1. Preguntar a usuario por valor de hora.
   1. Obtener de usuario valor de hora y asignar a variable `costoHora`.
   1. Preguntar a usuario por cantidad de días trabajados.
   1. Obtener de usuario dias y asignar a variable `dias`.
   1. Si `dias` es menor a `1` 0 mayor a `31`:
      1. Imprimo: Error, cantidad de días inválido.
      1. Preguntar a usuario nombre de empleado.
      1. Obtener nombre y asignarlo a variable `nombre`.
      1. Preguntar a usuario por edad (0 para finalizar).
      1. Obtener de usuario edad y asignar a variable `edad`.
   1. De lo contrario:
      1. Para `jornal` desde `1`; hasta `jornal` <= `dias`; incremento `jornal`
	 en `1`:
	 1. Preguntar cantidad de horas trabajadas:
	 1. Obtener de usuario cantidad de horas trabajadas y asignar a
	    varaible `horas`.
	 1. Si `horas` es mayor a `0` y menor a `25`:
	    1. Sumar `horas` a `totalHoras`.
	    1. Preguntar a usuario por recargo nocturno (`1` sí, `0` no):
	    1. Obtener indicador de recargo nocturno y asignar a varaible
	       `esNocturno`.
	    1. Si `esNocturno` es igual a 1:
	       1. Calculo `horas` multiplicado por `costoHora` y recargo de
		  `7%`, sumo resultado a `pago`.
	    1. De lo contrario:
	       1. Calculo `horas` multiplicado por `costoHora`, sumo resultado
		  a `pago`.
	 1. De lo contrario (cantidad de horas inválida):
	    1. Imprimo: error, cantidad de horas inválida. 
	    1. Decremento `jornal` en `1` para repetir día.
      1. Si `edad` es mayor o igual a `50` y `totalHoras` es mayor a `60`:
	 1. Calculo incremento de 1.5% sobre `pago`.
      1. Imprimo `nombre` y `pago`.
      1. Sumar `pago` a acumulado de `nomina`.
      1. Preguntar a usuario por nombre de empleado.
      1. Obtener nombre y asignarlo a variable `nombre`.
      1. Preguntar a usuario por edad (0 para finalizar).
      1. Obtener de usuario edad y asignar a variable `edad`.
1. Imprimir total de `nomina`.

## Refinamiento 3

1. Declarar variable `nombre`.
1. Declarar variable `edad`.
1. Declarar variable `nomina` y asignar valor de `0.0`.
1. Preguntar a usuario nombre de empleado.
1. Obtener nombre y asignarlo a variable `nombre`.
1. Preguntar a usuario por edad (0 para finalizar).
1. Obtener de usuario edad y asignar a variable `edad`.
1. Mientras `edad` sea mayor a `0`:
   1. Declarar variable `costoHora`.
   1. Declarar variable `dias`.
   1. Declarar variable `totalHoras` y asignar valor de `0`.
   1. Declarar variable `pago` y asignar valor de `0.0`.
   1. Preguntar a usuario por valor de hora.
   1. Obtener de usuario valor de hora y asignar a variable `costoHora`.
   1. Preguntar a usuario por cantidad de días trabajados.
   1. Obtener de usuario dias y asignar a variable `dias`.
   1. Si `dias` es menor a `1` o mayor a `31`:
      1. Imprimo: Error, cantidad de días inválido.
      1. Preguntar a usuario nombre de empleado.
      1. Obtener nombre y asignarlo a variable `nombre`.
      1. Preguntar a usuario por edad (0 para finalizar).
      1. Obtener de usuario edad y asignar a variable `edad`.
   1. De lo contrario:
      1. Para `jornal` desde `1`; hasta `jornal` <= `dias`; incremento `jornal`
	 en `1`:
	 1. Declarar variable `horas`.
	 1. Preguntar cantidad de horas trabajadas:
	 1. Obtener de usuario cantidad de horas trabajadas y asignar a
	    varaible `horas`.
	 1. Si `horas` es mayor a `0` y menor a `25`:
	    1. Sumar `horas` a `totalHoras`.
	    1. Declarar variable `esNocturno`.
	    1. Preguntar a usuario por recargo nocturno (`1` sí, `0` no):
	    1. Obtener indicador de recargo nocturno y asignar a varaible
	       `esNocturno`.
	    1. Si `esNocturno` es igual a 1:
	       1. Calculo `horas` multiplicado por `costoHora` y recargo de
		  `7%`, sumo resultado a `pago`.
	    1. De lo contrario:
	       1. Calculo `horas` multiplicado por `costoHora`, sumo resultado
		  a `pago`.
	 1. De lo contrario (cantidad de horas inválida):
	    1. Imprimo: error, cantidad de horas inválida. 
	    1. Decremento `jornal` en `1` para repetir día.
      1. Si `edad` es mayor o igual a `50` y `totalHoras` es mayor a `60`:
	 1. Calculo incremento de 1.5% sobre `pago`.
      1. Imprimo `nombre` y `pago`.
      1. Sumar `pago` a acumulado de `nomina`.
      1. Preguntar a usuario por nombre de empleado.
      1. Obtener nombre y asignarlo a variable `nombre`.
      1. Preguntar a usuario por edad (0 para finalizar).
      1. Obtener de usuario edad y asignar a variable `edad`.
1. Imprimir total de `nomina`.

# Empresa de Transportes

Don Pedro, dueño de la empresa de transportes TransAriporo Ltda., ha realizado
un estudio de campo y encontró que la cantidad de pasajeros que arriban a los
paraderos de la ruta circular que cubre su empresa cumple la siguiente
relación:

p(h) = h<sup>2</sup> - 24h + 144

> **Nota**: no existen medios pasajeros.

Donde `h` es la hora, como número entero, en formato de 24 horas y `p` es la
cantidad de pasajeros que arriban a los paraderos. Don Pedro quiere saber cómo
distribuir la salida de su flota transportadora a la hora en punto entre las
5:00 y 17:00 horas usando la capacidad máxima de sus vehículos, teniendo en
cuenta que cada recorrido circular toma una hora. 

La empresa cuenta con el siguiente parque automotor:

- 1 campero con capacidad de 7 pasajeros.
- 1 van con capacidad de 11 pasajeros.
- 1 chiva con capacidad de 21 pasajeros.

Escribe un programa que imprima en pantalla los horarios donde se muestre:

- Hora de partida.
- Cantidad de pasajeros transportados.
- Cantidad de pasajeros en espera.
- Vehículos utilizados.


Ejemplo:
```text
 Hora Trans.|Espe. Chiva|Van|Camp.
 5:00     39|   10     1|  1|   1
 6:00     39|    7     1|  1|   1
 7:00     32|    0     1|  1|   0
 8:00     11|    5     0|  1|   0
```


## Refinamiento 1

1. Para hora desde `5`; hasta hora menor o igual a `17`; incremento hora en
   `1`:
   1. Calculo pasajeros en espera para hora actual.
   1. Si pasajeros en espera es mayor igual a `21`:
      1. Uso chiva.
   1. Si pasajeros en espera es mayor igual a `11`:
      1. Uso van.
   1. Si pasajeros en espera es mayor igual a `7`:
      1. Uso campero.
   1. Imprimo hora de partida, cantidad de pasajeros transportados/espera y
      vehículos utilizados.

## Refinamiento 2

```mermaid
graph TD
    ini(("Inicio"))-->printHead[/"Imprimo<br>encabezado"/]
    printHead-->hourInit["hora = 5"]
    hourInit-->hourCheck{"hora <= 17"}
    hourCheck--"No"-->fin(("Fin"))
    hourCheck--"Sí"-->calcArr["Calculo pasajeros<br>en espera"]
    calcArr-->chivCheck{"pasEsp >= 21"}
    chivCheck--"Sí"-->chivUso[["Uso chiva"]]
    chivUso-->vanCheck{"pasEsp >= 11"}
    chivCheck--"No"-->vanCheck
    vanCheck--"Sí"-->vanUso[["Uso van"]]
    vanUso-->camCheck{"pasEsp >= 7"}
    vanCheck--"No"-->camCheck
    camCheck--"Sí"-->camUso[["Uso campero"]]
    camUso-->printRes[/"Imprimo: resultados<br>para hora."/]
    camCheck--"No"-->printRes
    printRes-->hourInc["hora++"]
    hourInc-->hourCheck
```

1. Inicializo variable `pasEsp` (pasajeros en espera) con valor `0`;
1. Imprimo encabezado de tabla.
1. Para `hora` desde `5`; hasta `hora` menor o igual a `17`; incremento `hora`
   en `1`:
   1. Inicializo variable `pasTra` (pasajeros transportados) con valor `0`.
   1. Inicializo variable `usoChi` (uso de chiva) con valor `0`.
   1. Inicializo variable `usoVan` (uso de van) con valor `0`.
   1. Inicializo variable `usoCam` (uso de campero) con valor `0`.
   1. Sumo `hora * hora - 24 * hora + 144` a `pasEsp`.
   1. Si `pasEsp` es mayor igual a `21`:
      1. Resto 21 a `pasEsp`.
      1. Sumo 21 a `pasTra`.
      1. Asigno `1` a `usoChi`.
   1. Si `pasEsp` es mayor igual a `11`:
      1. Resto 11 a `pasEsp`.
      1. Sumo 11 a `pasTra`.
      1. Asigno `1` a `usoVan`.
   1. Si `pasEsp` es mayor igual a `7`:
      1. Resto 7 a `pasEsp`.
      1. Sumo 7 a `pasTra`.
      1. Asigno `1` a `usoCam`.
   1. Imprimo `hora` , `pasTra`/`pasEsp` y `usoChi`/`usoVan`/`usoCam`.
